<?php
/***********************************************************
 *                 Retourne toutes les données             *
 *                 d'un point (Photos, articles)           *
 **********************************************************/
header('Content-type: text/json');
header("Content-type: application/json; charset=utf-8");

require("./../connect.inc.php");
$tab_pts;
$tab_pts_art;
$tab_pts_pht;

//Sélectionne tous les articles concernants un point
$query = "SELECT * FROM t_points NATURAL JOIN t_articles WHERE id_pts=" . $_POST["id_pts"];
$tab_pts_art=$pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
//Sélectionne que les photos du point si aucun article n'existe
if (!sizeof($tab_pts_art)) {
    $query = "SELECT * FROM t_points NATURAL JOIN t_photos WHERE id_pts=" . $_POST["id_pts"];
    $tab_pts = $pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
} 
//Sélectionne les photos et fusionne les tableaux des articles et des photos
else {
      $query = "SELECT * FROM t_points NATURAL JOIN  t_photos WHERE id_pts=" . $_POST["id_pts"];
    if(sizeof($pdo->query($query)->fetchAll(PDO::FETCH_ASSOC))){ 
        //Enregistre si les photos si il y en a
        $tab_pts_pht=$pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
        //Fusionne les deux tableaux
        $tab_pts=array_merge($tab_pts_art,$tab_pts_pht);
    }else{
        //Si aucune photo existe retourne le tableau des articles
        $tab_pts=$tab_pts_art;
    } 
}
//Encode en json le tableau de retour
 echo json_encode($tab_pts);
