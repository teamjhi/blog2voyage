<?php
/***********************************************************
 *           Ajoute un point dans la base                  *
 **********************************************************/
session_start();

header('Content-type: text/json');
header("Content-type: application/json; charset=utf-8");

require("./../connect.inc.php");
//Insère un nouveau point dans la base
$query="INSERT INTO `t_points`(`lat_pts`, `lng_pts`, `nom_pts`, `type_pts`, `id_vge`) VALUES (".$_POST["lat"].",".$_POST["long"].",\"".$_POST["name"]."\",\"".$_POST["type"]."\",".$_SESSION["id_vge"].")";

$pdo->query($query);
//Trouve le dernier ID insérer
$query="SELECT LAST_INSERT_ID() AS idPts FROM t_points GROUP BY idPts";
$id_pts=$pdo->query($query)->fetch(PDO::FETCH_ASSOC);
//Retourne le dernier id inséré
 echo json_encode($id_pts);
