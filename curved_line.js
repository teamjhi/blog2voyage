/* 
 
 Simple Javascript Curved Line function for use with Google Maps Api 
 
 author: Daniel Nanovski
 modifications: Coen de Jong
 version: 0.0.2 (Beta)
 website: http://curved_lines.overfx.net/
 
 License:
 Copyright (c) 2012 Daniel Nanovski, http://overfx.net/
 
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
 */
evenOdd = 0;
var num_trj = 0;
function curved_line_generate(map, Options) {
    num_trj++;
    tab_line[num_trj] = new Array();
    var Options = Options || {};
    var pStart = Options.pstart || null;
    var pStop = Options.pstop || null;
    var Color = Options.strokeColor || "#FF0000";
    var Opacity = Options.strokeOpacity || 1;
    var Weight = Options.strokeWeight || 2;
    var GapWidth = Options.gapWidth || 0;
    var Horizontal = Options.horizontal || false;
    var Multiplier = Options.multiplier || 2;
    var Resolution = Options.resolution || 0.05;
    var map = map; //Options.Map || null;
    var type = Options.type;
    var pt_1 = Options.id_pt_1;
    var pt_2 = Options.id_pt_2;

    var pos_start = get_position_pixel(map, pStart['marqueur'].position);
    var pos_stop = get_position_pixel(map, pStop['marqueur'].position);

//	alert(pos_stop.x);

    if (Horizontal == undefined) {

        Horizontal = true;

    }

    var angleDegrees = 0;

    //								|
    //						2		|		1
    //								|
    //                 -------------|--------------
    //								|
    //						3		|       4
    //								|

    /* 1 */
    if ((pos_start.x < pos_stop.x) && (pos_start.y < pos_stop.y)) {
        y = pos_stop.x - pos_start.x;
        x = pos_stop.y - pos_start.y;
        z = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
        angleDegrees = 90 + 180 * (Math.acos(y / z)) / Math.PI;
        //	alert("x "+x+" y"+y+" z"+z+" 1"+"----"+angleDegrees);
        //angleDegrees = 270;
    }
    /* 2 */
    if ((pos_start.x < pos_stop.x) && (pos_start.y > pos_stop.y)) {
        x = pos_stop.x - pos_start.x;
        y = pos_start.y - pos_stop.y;
        z = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
        angleDegrees = 180 * (Math.acos(y / z)) / Math.PI;
        //	alert("x "+x+" y"+y+" z"+z+" 2"+"----"+angleDegrees);
    }
    /* 3 */
    if ((pos_start.x > pos_stop.x) && (pos_start.y > pos_stop.y)) {
        x = pos_start.x - pos_stop.x;
        y = pos_start.y - pos_stop.y;
        z = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
        angleDegrees = 360 - (180 * (Math.acos(y / z)) / Math.PI);
        //	alert("x "+x+" y"+y+" z"+z+" 2"+"----"+angleDegrees);
    }
    /* 4 */
    if ((pos_start.x > pos_stop.x) && (pos_start.y < pos_stop.y)) {
        x = pos_start.x - pos_stop.x;
        y = pos_stop.y - pos_start.y;
        z = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
        angleDegrees = 180 + (180 * (Math.acos(y / z)) / Math.PI);
        //	alert("x "+x+" y"+y+" z"+z+" 2"+"----"+angleDegrees);
    }
//    alert(z);
    var echelle = 1;
    if (z < 100) {
        echelle = 1;
    } else if (z < 300) {
        echelle = 2;
    } else if (z < 500) {
        echelle = 3;
    } else if (z < 700) {
        echelle = 4;
    } else {
        echelle = 4;
    }
//	echelle = 4;

    var LastLat = pStart['marqueur'].position.lat();
    var LastLng = pStart['marqueur'].position.lng();

    var PartLat;
    var PartLng;

    var Points = new Array();
    var PointsOffset = new Array();

    for (point = 0; point <= 1; point += Resolution)
    {
        Points.push(point);
        offset = (0.6 * Math.sin((Math.PI * point / 1)));
        PointsOffset.push(offset);
    }

    var OffsetMultiplier = 0;

    if (Math.abs(pStop['marqueur'].position.lng() - pStart['marqueur'].position.lng()) > Math.abs(pStop['marqueur'].position.lat() - pStart['marqueur'].position.lat())) {
        Horizontal = 1;
    } else {
        Horizontal = 0;
    }

    if (Horizontal == 1) {

        var OffsetLenght = (pStop['marqueur'].position.lng() - pStart['marqueur'].position.lng()) * 0.1;

    } else {

        var OffsetLenght = (pStop['marqueur'].position.lat() - pStart['marqueur'].position.lat()) * 0.1;

    }

    var pt_middle = Math.abs(Points.length / 2);

    if (type == 'avion' && typeof type != 'undefined') {
        var titre = "Trajet en avion entre " + pStart['text'] + " et " + pStop['text']
        var couleur_contour = "#0000FF";
        var couleur_remplissage = "#0000FF";
    } else if (type == 'bus' && typeof type != 'undefined') {
        var titre = "Trajet en bus entre " + pStart['text'] + " et " + pStop['text']
        var couleur_contour = "#008A2A";
        var couleur_remplissage = "#008A2A";
    } else if (type == 'train' && typeof type != 'undefined') {
        var titre = "Trajet en train entre " + pStart['text'] + " et " + pStop['text']
        var couleur_contour = "#008A2A";
        var couleur_remplissage = "#008A2A";
    }



    var begin_trj = 1;
    for (var i = 0; i < Points.length; i++) {

        if (i == 4) {

            OffsetMultiplier = 1.5 * Multiplier;

        }

        if (i >= 5) {

            OffsetMultiplier = (OffsetLenght * PointsOffset[i]) * Multiplier;

        } else {

            OffsetMultiplier = (OffsetLenght * PointsOffset[i]) * Multiplier;

        }

        if (Horizontal == 1) {

            PartLat = (pStart['marqueur'].position.lat() + ((pStop['marqueur'].position.lat() - pStart['marqueur'].position.lat()) * Points[i])) + OffsetMultiplier;
            PartLng = (pStart['marqueur'].position.lng() + ((pStop['marqueur'].position.lng() - pStart['marqueur'].position.lng()) * Points[i]));

        } else {

            PartLat = (pStart['marqueur'].position.lat() + ((pStop['marqueur'].position.lat() - pStart['marqueur'].position.lat()) * Points[i]));
            PartLng = (pStart['marqueur'].position.lng() + ((pStop['marqueur'].position.lng() - pStart['marqueur'].position.lng()) * Points[i])) + OffsetMultiplier;

        }
        curved_line_create_segment(LastLat, LastLng, PartLat, PartLng, couleur_remplissage, Opacity, Weight, GapWidth, map, pt_1, pt_2, begin_trj);
        begin_trj = 0;
        // Ajout de la flèche de direction du trajet

        if (i == pt_middle + 1 && typeof type != 'undefined') {

            tab_line[num_trj][pt_1][pt_2][tab_line[num_trj][pt_1][pt_2].length] = new google.maps.Marker({
                map: map,
                zIndex: 40,
                title: titre,
                position: new google.maps.LatLng(PartLat, PartLng),
                icon: {
                    path: google.maps.SymbolPath.FORWARD_OPEN_ARROW,
                    fillColor: couleur_remplissage,
                    strokeColor: couleur_contour,
                    scale: echelle,
                    rotation: angleDegrees
                }
            });
        }


        LastLat = PartLat;
        LastLng = PartLng;

    }

    curved_line_create_segment(LastLat, LastLng, pStop['marqueur'].position.lat(), pStop['marqueur'].position.lng(), couleur_remplissage, Opacity, Weight, GapWidth, map, pt_1, pt_2);
    begin_trj = 0;


}

function curved_line_create_segment(LatStart, LngStart, LatEnd, LngEnd, Color, Opacity, Weight, GapWidth, map, pt_1, pt_2) {

    evenOdd++;

    if (evenOdd % (GapWidth + 1))
        return;

    var LineCordinates = new Array();

    LineCordinates[0] = new google.maps.LatLng(LatStart, LngStart);
    LineCordinates[1] = new google.maps.LatLng(LatEnd, LngEnd);
    if (typeof tab_line[num_trj][pt_1] == 'undefined') {
        tab_line[num_trj][pt_1] = new Array();
    }
    if (typeof tab_line[num_trj][pt_1][pt_2] == 'undefined') {
        tab_line[num_trj][pt_1][pt_2] = new Array();
    }

    tab_line[num_trj][pt_1][pt_2][tab_line[num_trj][pt_1][pt_2].length] = new google.maps.Polyline({
        path: LineCordinates,
        geodesic: false,
        strokeColor: Color,
        strokeOpacity: Opacity,
        strokeWeight: Weight
    });



    tab_line[num_trj][pt_1][pt_2][tab_line[num_trj][pt_1][pt_2].length - 1].setMap(map);


    /* console.log("---"+pt_1+" "+pt_2);
     if(tab_line[pt_1][pt_2].length == 5){
     console.log("supp "+pt_1+" "+pt_2);
     tab_line[pt_1][pt_2][4].setMap(null);
     }
     if(tab_line[pt_1][pt_2].length == 4){
     //      console.log("efface");
     tab_line[pt_1][pt_2][3].setMap(null);
     }
     if(tab_line[pt_1][pt_2].length == 6){
     //    console.log("efface");
     tab_line[pt_1][pt_2][5].setMap(null);
     }*/

}

function bound(value, opt_min, opt_max) {
    if (opt_min != null)
        value = Math.max(value, opt_min);
    if (opt_max != null)
        value = Math.min(value, opt_max);
    return value;
}

function degreesToRadians(deg) {
    return deg * (Math.PI / 180);
}

function radiansToDegrees(rad) {
    return rad / (Math.PI / 180);
}

/** @constructor */
function MercatorProjection() {
    this.pixelOrigin_ = new google.maps.Point(TILE_SIZE / 2,
            TILE_SIZE / 2);
    this.pixelsPerLonDegree_ = TILE_SIZE / 360;
    this.pixelsPerLonRadian_ = TILE_SIZE / (2 * Math.PI);
}

MercatorProjection.prototype.fromLatLngToPoint = function (latLng,
        opt_point) {
    var me = this;
    var point = opt_point || new google.maps.Point(0, 0);
    var origin = me.pixelOrigin_;

    point.x = origin.x + latLng.lng() * me.pixelsPerLonDegree_;

    // Truncating to 0.9999 effectively limits latitude to 89.189. This is
    // about a third of a tile past the edge of the world tile.
    var siny = bound(Math.sin(degreesToRadians(latLng.lat())), -0.9999,
            0.9999);
    point.y = origin.y + 0.5 * Math.log((1 + siny) / (1 - siny)) *
            -me.pixelsPerLonRadian_;
    return point;
};

MercatorProjection.prototype.fromPointToLatLng = function (point) {
    var me = this;
    var origin = me.pixelOrigin_;
    var lng = (point.x - origin.x) / me.pixelsPerLonDegree_;
    var latRadians = (point.y - origin.y) / -me.pixelsPerLonRadian_;
    var lat = radiansToDegrees(2 * Math.atan(Math.exp(latRadians)) -
            Math.PI / 2);
    return new google.maps.LatLng(lat, lng);
};

function get_position_pixel(map, point) {
    var numTiles = 1 << map.getZoom();
    var projection = new MercatorProjection();
    var worldCoordinate = projection.fromLatLngToPoint(point);

    var pixelCoordinate = new google.maps.Point(
            worldCoordinate.x * numTiles,
            worldCoordinate.y * numTiles
            );

    var tileCoordinate = new google.maps.Point(
            Math.floor(pixelCoordinate.x / TILE_SIZE),
            Math.floor(pixelCoordinate.y / TILE_SIZE)
            );

    tab = new Array();
    tab['x'] = Math.floor(pixelCoordinate.x);
    tab['y'] = Math.floor(pixelCoordinate.y);

    return(tab);
}
