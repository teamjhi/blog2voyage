<!DOCTYPE html>
<?php
/*********************************************************
 *                  Création d'articles                  *
 ********************************************************/
session_start();
require("./../fonction.php");
?>
<html>
    <head>
        <!-- CSS de la feuille -->
        <style>
            textArea{
                height: 300px;
            }
        </style>
        <!-- Feuille CSS et Fichiers JS -->
        <link rel="stylesheet" href="../jquery-ui/jquery-ui.min.css"/>       
        <script src="../jquery-ui/external/jquery/jquery.js"></script>
        <script src="../jquery-ui/jquery-ui.min.js"></script>
        <script src="../jquery-ui/jquery.ui.datepicker-fr.js" type="text/javascript"></script>
        <script src="./../fonction.js"></script>
        <script type="text/javascript" src="./../tinymce/tinymce.min.js"></script>                    
        <script type="text/javascript">

            $(function() {
                //Paramétrage éditeur de texte
                tinymce.init({
                    selector: "#newArt, .article",
                    plugins: [
                        "advlist autolink lists link image charmap print preview anchor",
                        "searchreplace visualblocks code fullscreen",
                        "insertdatetime media table contextmenu paste moxiemanager"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image insertimage "
                });

                //Calendrier début article
                $("#datepickerDb").datepicker({
                    beforeShow: startCalendarDb
                });
                //Calendrier fin article
                $("#datepickerFin").datepicker({
                    beforeShow: startCalendarFin
                });

                //Calendrier de fin débute au maximum à la date du calendrier de début
                function startCalendarFin() {
                    return{minDate: $("#datepickerDb").datepicker('getDate')};
                }
                //Si le calendrier de fin et deja rempli le calendrier de début ne peut pas dépasser cette date
                function startCalendarDb() {
                    if ($("#datepickerFin").datepicker('getDate') != "") {
                        return{maxDate: $("#datepickerFin").datepicker('getDate')};
                    }
                }
            });
        </script>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //Nouvel article
        if (isset($_POST["new"])) {
            $_SESSION["id_pts"] = $_POST["new"];
            echo "<form method=\"post\" action=\"\">";
            echo "<textarea id =\"newArt\" name=\"content\" style=\"width:100%\"></textarea><br />";
            echo "<label>Date de début</label><input id=\"datepickerDb\" type=\"text\" name=\"startArt\"/>";
            echo "<label>Date de fin</label><input id=\"datepickerFin\" type=\"text\" name=\"endArt\"/>";
            echo "<input type=\"submit\" name=\"enregistrer\" value=\"Enregistrer\"/>";
            echo "<input type=\"button\" value=\"Retour\" onclick=\"changePage('articles')\"/>";
            echo "</form >";
        }
        //Enregistrement du nouvel article
        else if (isset($_POST["enregistrer"])) {
            $startArt = implode("-", explode("/", $_POST["startArt"]));
            $endArt = implode("-", explode("/", $_POST["endArt"]));
            addArticle($_SESSION["id_pts"], $_POST["content"], $startArt, $endArt);
            ?>
            <script>
            <?php
            //Après enregistrement retour à la partie avec tous les points
            echo "window.location=\"articles.php\";";
            ?>
            </script>
            <?php
        }
        ?>
    </body>
</html>
