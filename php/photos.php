﻿<!DOCTYPE html>
<?php
/*********************************************************
 *                   Modification photos                 *
 ********************************************************/
session_start();
require("../fonction.php");
?>
<html>
    <head>
        <!-- Feuille CSS et Fichiers JS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="../jquery-ui/jquery-ui.min.css"/>
        <script src="../jquery-ui/external/jquery/jquery.js"></script>
        <script src="../jquery-ui/jquery-ui.min.js"></script>
        <script src="../jquery-ui/jquery.ui.datepicker-fr.js" type="text/javascript"></script>
        <script src="./../fonction.js"></script>
        <script type="text/javascript" src="./../tinymce/tinymce.min.js"></script>
        <meta charset="UTF-8">
        <title></title>

    </head>
    <script>
        //Plugin Jquery affchage des points
        $(function () {
            $("#accordion, .photo").accordion({
                collapsible: true,
                active: false,
                heightStyle: "content"
            });
        });

    </script>
    <body>
        <?php
        //Sauvegarde modification article
        if (isset($_POST["save"])) {
            updatePhoto($_POST["latPht"], $_POST["longPht"], $_POST["descPht"], $_POST["idPts"], $_POST["datePht"], $_POST["nomPht"]);
        } 
        //Suppression articles
        else if (isset($_POST["delPht"])) {
            deletePhoto($_POST["nomPht"]);
        }
        //Retour page admin
        echo "<input type=\"button\" value=\"Retour\" onclick=\"changePage('admin')\" class=\"btn btn-primary\"/>";
        echo "<div id=\"accordion\">";
        //Affichage des points
        foreach (travelPoints($_SESSION["id_vge"]) as $key => $pts) {
            echo "<h3>" . $pts["nom_pts"] . "</h3>";
            echo "<div>";
            //Affichage des photos des points
            foreach (photosPoint($pts["id_pts"]) as $key => $pht) {
                echo "<div>";
                echo "<form action=\"\" method=\"post\">";
                echo "<img src=\"./images/" . $pht["nom_pht"] . "\" width=150px height=150px/>";
                echo "<textarea class=\"photo\" name=\"descPht\" cols=\"30\" rows=\"3\">" . $pht['desc_pht'] . "</textarea>";
                echo "<input type=\"hidden\" name=\"nomPht\" value=\"" . $pht["nom_pht"] . "\"/>";
                echo "<input type=\"hidden\" name=\"latPht\" value=\"" . $pht["lat_pht"] . "\"/>";
                echo "<input type=\"hidden\" name=\"longPht\" value=\"" . $pht["lng_pht"] . "\"/>";
                echo "<input type=\"hidden\" name=\"datePht\" value=\"" . $pht["date_heure_pht"] . "\"/>";
                echo "<input type=\"hidden\" name=\"idPts\" value=\"" . $pts["id_pts"] . "\"/>";
                echo "<br /><input type=\"submit\" value=\"Sauvegarder\" name=\"save\"/>";
                echo "<input type=\"submit\" value=\"Supprimer\" name=\"delPht\"/>";
                echo "</form>";
                echo "</div>";
            }
            echo "</div>";
        }
        echo "</div>";

        //Retour à la page d'admin
        echo "<input type=\"button\" value=\"Retour\" onclick=\"changePage('admin')\" class=\"btn btn-primary\"/>";
        ?>
        <div id="newPts"></div>
    </body>
</html>
