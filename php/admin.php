<?php
/* * *********************************************************
 *                      Page principale                    *
 * ******************************************************** */
session_start();
require("./../connect.inc.php");
require("../fonction.php");
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">

    <head>

        <title>Blog2voyage</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

        <!-- Feuilles CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
            <link rel="stylesheet" type="text/css" href="./../icon/css/map-icons.css" />
            <link rel="stylesheet" href="./../jQRangeSlider/css/iThing.css" type="text/css" />
            <link rel="stylesheet" href="./../css/style.css" type="text/css" />
            <link rel="stylesheet" href="./../jquery-ui/jquery-ui.css" type="text/css" />
            <!-- CSS de cette page -->
            <style>
                body{
                    background-color:#B9C3E0;
                }
                #drop-area{
                    margin-top: 15px;
                    margin-bottom: 15px;
                    border:2px dashed black;
                    float: left;
                    background: #D8F9D3;
                    height:150px;
                    width:300px;
                }              
                h3.drop-text{
                    text-align: center;
                }
                #voyage{
                    width:100px;
                    height:30px;
                    font-size: 17px;
                    margin-bottom: -20px;
                }             

            </style>
            <!-- Feuilles JS et de l'API Maps-->
            <script src="./../jquery/jquery.min.js"></script>
            <script src="./../jquery-ui/jquery-ui.js"></script>
            <script src="./../jQRangeSlider/jQRangeSlider-min.js"></script>
            <script src="./../jQRangeSlider/jQDateRangeSlider-withRuler-min.js"></script>          
            <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
            <script type="text/javascript" src="../curved_line.js"></script>
    </head>

    <?php
    $tab_trj;
    //Les types de points
    $tab_type_pts = ["plane", "homegardenbusiness", "rail", "bus"];
    //Les types de trajets
    $tab_type_trj = ["avion", "bus", "train"];

    //Supprime un voyage
    if (isset($_POST["delVge"])) {
        echo "<form action=\"\" method=\"post\"><label>Voulez-vous vraiment le supprimer?</label><br/><input type=\"submit\" name=\"YesDelVge\" value=\"Oui\"/>"
            . "<input type=\"submit\" name=\"NoDelVge\" value=\"Non\"/></form>";
        
    } else if (isset($_POST["YesDelVge"])) {
        $query = "DELETE FROM `t_voyage` WHERE id_vge=" . $_SESSION["id_vge"] . "";
        $pdo->query($query);
        unset($_SESSION["id_vge"]);
    }
    //Changement de voyage
    if (isset($_POST["voyage"])) {
        //Nouveau voyage
        if ($_POST["voyage"] == "new") {
            echo "<form method=\"post\" action=\"\">";
            echo "<label> Nom du nouveau voyage</label><br />";
            echo "<input type=\"text\" name=\"travel\"/><br />";
            echo "<input type=\"submit\" name=\"addTravel\" value=\"Ajouter\" class=\"btn btn-default\"/>";
            echo "<input type=\"submit\" onclick=\"changePage('admin')\" value=\"Retour\" class=\"btn btn-default\"/>";
            echo "</form>";
        }
        //Sinon récupéré l'id envoyé
        else {
            $id_vge = $_POST["voyage"];
            $_SESSION["id_vge"] = $id_vge;
        }
    }
    //Si un nouveau voyage est ajouté
    else if (isset($_POST["addTravel"])) {
        addTravel($_POST["travel"]);
        $id_vge = lastIdTravel();
        $_SESSION["id_vge"] = $id_vge;
    }
    //Si un voyage est déja en cours de modification
    else if (isset($_SESSION["id_vge"])) {
        $id_vge = $_SESSION["id_vge"];
    }
    //Voyage par défaut
    else {
        $id_vge = 5;
        $_SESSION["id_vge"] = $id_vge;
    }

    //Sauvegarde les nouvelles photos
    if (isset($_POST["save"])) {
        foreach ($_POST["pht"] as $key => $pht) {
            $tab_infoPts = infoPoint($pht["pts"]);
            updatePhoto($tab_infoPts["lat_pts"], $tab_infoPts["lng_pts"], $pht["desc"], $tab_infoPts["id_pts"], $tab_infoPts["date_heure_db_art"], $pht["namePhoto"]);
        }
    }
    //Supprime les photos ajoutées mais pas sauvegardées
    else if (isset($_POST["annuler"])) {
        foreach ($_POST["pht"] as $key => $pht) {
            deletePhoto($pht["namePhoto"]);
            unlink("./images/" . $pht["namePhoto"]);
        }
    }
    ?>
    <script>
        // Initialisation des variables JS
        var tab_pos = new Array();
        var tab_line = new Array();
        var marker = new Array();
        var tab_new = new Array();
        var carte;
        var lat;
        var long;
        var range_db;
        var range_fin;
        var bounds = new google.maps.LatLngBounds();
        TILE_SIZE = 256;
        var months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
        var id_dep = "";
        var id_arr = "";
        var delTrj;
        var ptsSelected;
        var tab_trj = new Array();
        var vge;
        var updatePeriode;
<?php
//Recherche tous les trajets du voyage
$tab_trj = $pdo->query("SELECT * FROM t_trajets JOIN t_points ON dp_trj=id_pts WHERE id_vge=" . $id_vge)->fetchAll(PDO::FETCH_ASSOC);
// Recherche de la date min et max du voyage  
$tab_range = $pdo->query("SELECT MIN(date_heure_db_art) AS date_db ,Max(date_heure_fin_art) AS date_fin FROM t_points NATURAL JOIN t_articles WHERE date_heure_db_art<>'00-00-00 00:00:00' AND date_heure_fin_art<>'00-00-00 00:00:00' AND id_vge=" . $id_vge)->fetch(PDO::FETCH_ASSOC);
if (sizeof($tab_range)) {
    $t = strtotime($tab_range["date_db"]);
    echo "range_db= new Date(" . date("Y", $t) . "," . (date("m", $t) - 1) . "," . date("d", $t) . ");";
    $t = strtotime($tab_range["date_fin"]);
    echo "range_fin= new Date(" . date("Y", $t) . "," . (date("m", $t) - 1) . "," . date("d", $t) . ");";
}
?>

        function initialiser() {
            $("#slider").remove();
<?php
//Recherche tous les points
$tab_pts = $pdo->query("SELECT * FROM t_points WHERE id_vge=" . $id_vge)->fetchAll(PDO::FETCH_ASSOC);
foreach ($tab_pts AS $pts) {
    //Affiche tous les points sur la carte
    echo "tab_pos[" . $pts['id_pts'] . "] = new Array();\n";
    echo "tab_pos[" . $pts['id_pts'] . "] = new_point(" . $pts['lat_pts'] . "," . $pts['lng_pts'] . ",\"" . $pts['type_pts'] . "\",\"" . $pts['nom_pts'] . "\");\n";
}

//Si des trajets existent
if (sizeof($tab_trj)) {
    foreach ($tab_trj as $key => $trj) {
        //Affichage et stockage des trajets
        echo "new_trajet(carte,\"" . $trj['type_trj'] . "\"," . $trj['dp_trj'] . "," . $trj['ar_trj'] . ");\n";
        echo "tab_trj[" . $key . "]=new Array();\n";
        echo "tab_trj[" . $key . "]['type_trj']=\"" . $trj['type_trj'] . "\";\n";
        echo "tab_trj[" . $key . "]['dp_trj']=" . $trj['dp_trj'] . ";\n";
        echo "tab_trj[" . $key . "]['ar_trj']=" . $trj['ar_trj'] . ";\n";
    }
}
?>
            // Adapte la carte à l'écran et au contenu
            carte.fitBounds(bounds);
            //Récupère l'id du voyage stocké dans la variable php
            vge =<?php echo $id_vge; ?>;

            //Met par défaut le voyage sélectionné dans la liste déroulante
            $("#voyage option[value=" + vge + "]").prop("selected", true);

            //Ajout d'événement clique droit sur les points pour faire un nouveau trajet ou pour supprimer le point
            tab_pos.forEach(function (d) {
                var marker = tab_pos[tab_pos.indexOf(d)]["marqueur"];
                google.maps.event.addListener(marker, 'click', function () {
                    //Ouvre la boîte de dialogue si la création d'un nouveau voyage n'est pas en cours
                    if ($("#newTrj").is(":not(:checked)")) {
                        ptsSelected = tab_pos.indexOf(d);
                        $("#optPoint").dialog("open");
                    } else {
                        //Si un nouveau voyage est en cours de création stock les id des points sélectionnés
                        if (id_arr === "" && id_dep !== "" && id_dep !== tab_pos.indexOf(d)) {
                            id_arr = tab_pos.indexOf(d);
                            $("#addTrajet").dialog("open");
                        }
                        //Si deux points différents ont été sélectionné demande le type de trajet entre les deux points
                        else {
                            $("#optPoint").dialog("open");
                        }

                    }
                });
            });
            //Changement de voyage
            $('#voyage').change(function () {
                $("#formVoyage").submit();
            });
            //Ajout d'événement sur les trajets pour les supprimés
            tab_line.forEach(function (tab_depPts) {
                if (typeof tab_depPts != 'undefined') {
                    tab_depPts.forEach(function (tab_arrPts) {
                        if (typeof tab_arrPts != 'undefined') {
                            tab_arrPts.forEach(function (tab_seg) {
                                tab_seg.forEach(function (segment) {
                                    google.maps.event.addListener(segment, 'click', function () {
                                        //Stocke le numéro unique du trajet
                                        delIdTrj = tab_line.indexOf(tab_depPts);
                                        //Ouvre la boîte de dialogue
                                        $("#deleteTraj").dialog("open");
                                    });
                                });
                            });
                        }
                    });
                }
            });
            //Ajout d'événement sur la carte pour ajouter des pionts
            google.maps.event.addListener(carte, 'click', function (event) {
                //Récupère les données GPS
                var latlong = event.latLng;
                //Récupère la latitude
                lat = latlong.lat();
                //Récupère la longitude
                long = latlong.lng();
                //Ouvre boîte de dialogue
                $("#addPoint").dialog("open");
            });
        }//fin initialiser()

        $(function () {
           
            //Paramétrage boîte de dialogue pour l'ajout de point
            $("#addPoint").dialog({
                autoOpen: false,
                beforeClose: function () {
                    $("#new").val("");
                },
                width: 350,
                buttons: [
                    {
                        text: "OK",
                        click: function () {
                            //Récupération des données de la boîte de dialogue
                            var name = $("#new").val();
                            var type = $("select[name='type_pts'] > option:selected").val();
                            //Récupération de toutes les informations
                            var formImage = new FormData();
                            formImage.append("lat", lat);
                            formImage.append("long", long);
                            formImage.append("name", name);
                            formImage.append("type", type);
                            //Envoi des données au fichier PHP pour être ajoutées dans la base
                            $.ajax({
                                url: "./../json/addPoint.json.php",
                                type: "POST",
                                data: formImage,
                                dataType: 'json',
                                contentType: false,
                                cache: false,
                                processData: false,
                                success: function (data) {
                                    //Ajout des point dans le tableau conteanant tous les points
                                    add_point(data.idPts, lat, long, type, name);
                                    //Leur ajoute un événement pour soit les supprimer soit leur ajouter un trajet
                                    marker_update(data.idPts);
                                }
                            });
                            $(this).dialog("close");
                        }

                    }
                ]

            });
            //Paramétrage boîte de dialogue ajout d'un trajet sur la carte et dans la base
            $("#addTrajet").dialog({
                autoOpen: false,
                beforeClose: function () {
                    id_dep = "";
                    id_arr = "";
                },
                width: 350,
                buttons: [
                    {
                        text: "OK",
                        click: function () {
                            //Récupère le type de teajet de la boîte
                            var type = $("select[name='type_trj'] > option:selected").val();
                            //Crée un nouveau trajet
                            new_trajet(carte, type, id_dep, id_arr);
                            //Lui ajoute un événement
                            update_event_trj();
                            //Récupère et envoi les données au fichier PHP
                            var formImage = new FormData();
                            formImage.append("dep", id_dep);
                            formImage.append("arr", id_arr);
                            formImage.append("type", type);
                            $.ajax({
                                url: "./../json/newTrajet.json.php",
                                type: "POST",
                                data: formImage,
                                dataType: 'json',
                                contentType: false,
                                cache: false,
                                processData: false,
                                success: function () {
                                }
                            });
                            $(this).dialog("close");
                            $("#newTrj").attr("checked", false);
                        }
                    }
                ]
            });
            //Paramétrage boîte de dialogue suppression d'un trajet sur la carte et dans la base
            $("#deleteTraj").dialog({
                autoOpen: false,
                width: 350,
                buttons: [{
                        text: "Oui",
                        click: function () {
                            //Différencier changement de période et suppresion de point
                            updatePeriode = false;
                            var formTrj = new FormData();
                            //Récupère numéro départ et arrivée du trajet
                            tab_line[delIdTrj].forEach(function (d) {
                                for (key in tab_line[delIdTrj]) {
                                    formTrj.append("dp_trj", key);
                                }
                                d.forEach(function () {
                                    for (key in d) {
                                        formTrj.append("ar_trj", key);
                                    }
                                });
                            });
                            //Supprime le trajet
                            tab_line[delIdTrj].forEach(function (tab_2) {
                                if (typeof tab_2 != 'undefined') {
                                    tab_2.forEach(function (d2) {

                                        d2.forEach(function (segment) {
                                            segment.setMap(null);
                                        });
                                    });
                                }
                            });
                            //Envoi le point de départ et d'arrivée au fichier PHP pour être supprimé
                            $.ajax({
                                url: "./../json/delTrajet.json.php",
                                type: "POST",
                                data: formTrj,
                                dataType: 'json',
                                contentType: false,
                                cache: false,
                                processData: false,
                                success: function () {
                                }
                            });
                            $(this).dialog("close");
                        }
                    }, {
                        text: "Non",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ]
            });
            //Paramétrage boîte de dialogue suppression du point soit un nouveau trajet
            $("#optPoint").dialog({
                autoOpen: false,
                width: 350,
                buttons: [{
                        text: "Ok",
                        click: function () {
                            //Suppression d'un point
                            if ($("#delPts").is(":checked")) {
                                var formPoint = new FormData();
                                formPoint.append("idPts", ptsSelected);
                                updatePeriode = false;
                                remove_point(ptsSelected);
                                //Delete point dans la base
                                $.ajax({
                                    url: "./../json/delPoint.json.php",
                                    type: "POST",
                                    data: formPoint,
                                    dataType: 'json',
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    success: function () {
                                    }
                                });
                                $("#delPts").attr("checked", false);
                            }
                            //Nouveau trajet stock l'id du point de départ
                            else if ($("#newTrj").is(":checked")) {

                                id_dep = ptsSelected;
                                id_arr = "";
                            }
                            $(this).dialog("close");
                        }
                    }
                ]
            });
        });</script>

    <!-- Structure DOM  -->
    <body>
        <div class="container-fluid">        
            <div id="content">
                <?php
                //Choix du voyage
                echo "<br /><form id=\"formVoyage\" method=\"post\" action=\"\">";
                echo "<select id=\"voyage\" name=\"voyage\">";
                foreach (allTravels() as $key => $value) {
                    echo "<option value=\"" . $value["id_vge"] . "\"/>" . $value["nom_vge"] . "</option>";
                }
                echo "<option  value=\"new\" />Nouveau voyage</option>";
                echo "</select></form><br /><form action=\"\" method=\"post\"><input type=\"submit\" value=\"Supprimer ce voyage\" name=\"delVge\" class=\"btn btn-default\"/></form>";

                //Menu ajout / modification d'articles, modification photos et affichage
                showButtons();
                //Zone drag and drop photos
                echo "<div id=\"drop-area\"><h3 class=\"drop-text\">Glissez-déposez vos images ici</h3></div>";
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div id="carte" style="width:100%; height:85%"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div id="slider"></div>
                    </div>
                </div>              
            </div>
        </div>
        <br />
        <!--Boîte de dialogue pour l'ajout de point-->
        <div id="addPoint">
            <?php
            echo "<table>";
            echo "<tr><td>Nom du nouveau point </td><td><input id=\"new\" type=\"text\"/></td></tr>";
            echo "<tr><td>Type du point </td><td><select name=\"type_pts\">";
            foreach ($tab_type_pts as $key => $trj) {
                echo "<option value=\"" . $trj . "\">" . $trj . "</option>";
            }
            echo "</select></td></tr>";
            echo "</table>";
            ?>
        </div>
        <!--Boîte de dialogue pour l'ajout de trajet-->
        <div id="addTrajet">
            <?php
            echo "<table>";
            echo "<tr><td>Transport utilisé </td><td><select name=\"type_trj\">";
            foreach ($tab_type_trj as $key => $trj) {
                echo "<option value=\"" . $trj . "\">" . $trj . "</option>";
            }
            echo "</select></td></tr>";
            echo "</table>";
            ?>
        </div>
        <!--Boîte de dialogue pour la suppression de trajet-->
        <div id="deleteTraj">
            <?php
            echo "<table>";
            echo "<tr><td>Voulez-vous supprimer ce trajet?</td></tr>";

            echo "</table>";
            ?>
        </div>
        <!--Boîte de dialogue pour la suppresion de point ou de l'ajout de nouveau trajet-->
        <div id="optPoint">
            <?php
            echo "<table>";
            echo "<tr><td>Supprimer le point</td><td><input id=\"delPts\" type=\"radio\" name=\"optPts\"/></td></tr>";
            echo "<tr><td>Nouveau trajet</td><td><input id=\"newTrj\" type=\"radio\" name=\"optPts\"/></td></tr>";
            echo "</table>";
            ?>
        </div> 
        
    </body>

</html>
<script src="./../fonction.js"></script>


