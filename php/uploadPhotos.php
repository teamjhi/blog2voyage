<!-- Feuille JS pour plugin JQuery -->
<script src="../jquery/jquery.min.js"></script>
<?php
/*********************************************************
 *     Récupération et retourne les infos des photos     *
 ********************************************************/
session_start();
require ("./../fonction.php");

$tab_img;
$tab_pts;
if (is_array($_FILES)) {
    //Parcours chaque photos envoyées
    for ($i = 0; $i < sizeof($_FILES); $i++) {

        $sourcePath = $_FILES['userImage' . $i]['tmp_name'];
        $targetPath = "./images/" . $_FILES['userImage' . $i]['name'];
        //Met les photos dans le dossier ./images
        if (move_uploaded_file($sourcePath, $targetPath)) {
            //Récupère les données exif
            $exif = exif_read_data($targetPath, 0, true);
            //Toutes les infos à récupéré
            $tab_img[$i]["name"] = "";
            $tab_img[$i]["date"] = "";
            $tab_img[$i]["desc"] = "";
            $tab_img[$i]["id_pts"] = "";
            $tab_img[$i]["latitude"] = "";
            $tab_img[$i]["longitude"] = "";
            //Récupère et transforme les données GPS de la photos si elles existent
            if (isset($exif["GPS"])) {
                $tab_img[$i]["latitude"] = DMStoDD($exif["GPS"]["GPSLatitude"][0], $exif["GPS"]["GPSLatitude"][1], $exif["GPS"]["GPSLatitude"][2], $exif["GPS"]["GPSLatitudeRef"]);
                $tab_img[$i]["longitude"] = DMStoDD($exif["GPS"]["GPSLongitude"][0], $exif["GPS"]["GPSLongitude"][1], $exif["GPS"]["GPSLongitude"][2], $exif["GPS"]["GPSLongitudeRef"]);
            }
            //Récupère la date de prise de vue de la photo si elle existe
            if (isset($exif["EXIF"]["DateTimeOriginal"])) {
                $tab_img[$i]["date"] = $exif["EXIF"]["DateTimeOriginal"];
            }
            //Récupère l'id de la photo précédente pour renommer la nouvelle photo sinon prend l'id 0
            if (lastIdPhoto() == "") {
                $id_pht = 0;
            } else {
                $id_pht = lastIdPhoto();
            }
            //Récupère le nom du voyage et enlève les accents
            $nameTra = minusculesSansAccents(nameTravel($_SESSION["id_vge"]));
            //Renomme l'image
            rename($targetPath, "./images/" . $nameTra . "-" . $id_pht . ".jpg");
            //Renomme l'image dans le tableau les stockant
            $tab_img[$i]["name"] = $nameTra . "-" . $id_pht . ".jpg";
            addPhoto("", "", "", "", "", $tab_img[$i]["name"]);
        }
    }
    //Ajout d'une description et du point à associer par l'utilisateur
    echo "<form method=\"post\" action=\"\"><table>";
    foreach ($tab_img as $key => $img) {
        echo "<tr><td><img src=\"images/" . $img["name"] . "\" width=150px height=150px/></td>";
        //Classe les points du voyage selon les données GPS de la photo
        if (!empty($img["latitude"]) && !empty($img["longitude"])) {
            $tab_pts = classementPoints($_SESSION["id_vge"], $img["latitude"], $img["longitude"]);
        } 
        //Si pas GPS classe selon la date des articles du voyage
        else if (!empty($img["date"])) {//Changer date car en phase test
            $tab_pts = classementDate($_SESSION["id_vge"], $img["date"]);
        } 
        //Si aucune donnée affiche simplement tous les points
        else {
            $tab_pts = travelPoints($_SESSION["id_vge"]);
        }
        //Liste déroulante avec tous les points
        echo "<td><SELECT   name=\"pht[" . $img["name"] . "][pts]\">";
        foreach ($tab_pts as $key => $pts) {
            echo "<OPTION value=\"" . $pts["nom_pts"] . "\">" . $pts["nom_pts"] . "</OPTION>";
        }       
        echo "</SELECT>";
        //Zone de texte pour description de la photo
        echo "</td><td></td><td><textarea name=\"pht[" . $img["name"] . "][desc]\" cols=\"30\" rows=\"2\"></textarea></td>";
        echo "<td><input type=\"hidden\" name=\"pht[" . $img["name"] . "][namePhoto]\" value=\"" . $img["name"] . "\"</td></tr>";
    }
    //Sauvegarder les photos
    echo "<tr><td><input type=\"submit\" name=\"save\" value=\"Sauvegarder\"/></td>";
    //Annuler l'ajout des photos
    echo "<td><input type=\"submit\"  value=\"Annuler\" name=\"annuler\"/></td></tr>";

    echo "</table>";
    echo "</form>";
}
?>
 
