<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
/*********************************************************
 *           Ajout, modification d'articles              *
 ********************************************************/
session_start();
require("./../fonction.php");
?>
<head>
    <!-- Feuille CSS -->
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"/>
    <!-- CSS de cette page -->
    <style>
        div{
            padding-bottom: 25px;
            padding-top: 25px;
        }
        p{
            margin-bottom: -20px;
        }
        .btn btn-default{
            border:2px solid black;
        }

    </style>
    <!-- Feuille CSS et fichiers JS -->
    <link rel="stylesheet" href="../jquery-ui/jquery-ui.min.css"/>
    <script src="../jquery-ui/external/jquery/jquery.js"></script>
    <script src="../jquery-ui/jquery-ui.min.js"></script>
    <script src="../jquery-ui/jquery.ui.datepicker-fr.js" type="text/javascript"></script>
    <script src="./../fonction.js"></script>
    <script type="text/javascript" src="./../tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        $(function() {
            //Paramétrage éditeur de texte
            tinymce.init({
                selector: "#newArt, .article",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste moxiemanager"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image insertimage "
            });

            //Calendrier début article
            $(".datepickerDb").datepicker({
                // beforeShow: startCalendarDb
            });
            //Calendrier fin article
            $(".datepickerFin").datepicker({
                //beforeShow: startCalendarFin
            });
            //Plugin JQuery affichage des articles
            $("#accordion, .articles").accordion({
                collapsible: true,
                active: false,
                heightStyle: "content"
            });
            //Ajout d'événement pour les nouveaux articles         
            $("#accordion").on("click", ".newArt", function() {
                $("#ghost_form").append("<input type=\"hidden\" name=\"new\" value=\"" + $(this).attr("id_pts") + "\"/>");
                $("#ghost_form").submit();


            });
         
        });

    </script>
    <meta charset="UTF-8"></meta>
</head>
<body> 
    <?php
    //Retour à la page d'admin
    echo "<input type=\"button\" value=\"Retour\" onclick=\"changePage('admin')\" class=\"btn btn-primary\"/>";
    //SAuvegarde modifications d'article
    if (isset($_POST["save"])) {
        updateArticle($_POST["idArt"], $_POST["startArt"], $_POST["endArt"], $_POST["article"], $_POST["idPts"]);
    } 
    //Suppression d'articles
    else if (isset($_POST["delArt"])) {
        deleteArticle($_POST["idArt"]);
    }
    //Affichage de tous les points du voyage
    echo "<div id=\"accordion\">";
    foreach (travelPoints($_SESSION["id_vge"]) as $key => $pts) {
        echo "<h3>" . $pts["nom_pts"] . "</h3>";
        echo "<div>";
        echo "<input id_pts=\"" . $pts["id_pts"] . "\" type=\"button\" class=\"newArt\" value=\"Nouvel article\"/><br />";
        //Ajout des articles dans chaque point
        foreach (articlesPoint($pts["id_pts"]) as $key => $art) {
            echo "<div>";
            echo "<form action=\"\" method=\"post\">";
            echo "<textarea class=\"article\" name=\"article\">" . $art['article'] . "</textarea>";
            echo "Date de début <input class=\"datepickerDb\" type=\"text\" name=\"startArt\" value=\"" . substr($art["date_heure_db_art"], 0, 10) . "\"/> - "
            . "Date de fin <input class=\"datepickerFin\" type=\"text\" name=\"endArt\" value=\"" . substr($art["date_heure_fin_art"], 0, 10) . "\"/><br />";
            echo "<input type=\"hidden\" name=\"idArt\" value=\"" . $art["id_art"] . "\"/>";
            echo "<input type=\"hidden\" name=\"idPts\" value=\"" . $pts["id_pts"] . "\"/>";
            echo "<br /><input type=\"submit\" value=\"Sauvegarder\" name=\"save\"/>";
            echo "<input type=\"submit\" value=\"Supprimer\" name=\"delArt\"/>";
            echo "</form>";
            echo "</div>";
        }
        echo "</div>";
    }

    echo "</div>";
    //Retour à la page d'admin
    echo "<input type=\"button\" value=\"Retour\" onclick=\"changePage('admin')\" class=\"btn btn-primary\"/>";
    ?>
    <!-- Formulaire pour la création de nouveau articles -->
    <form id="ghost_form" action='./newArt.php' method='post'></form>
</body>
</html>
