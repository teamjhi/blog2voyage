//Définit les différentes options de la carte
var options = {
    //center: latlng,
    zoom: 6
};

//Création de la carte Maps
carte = new google.maps.Map(document.getElementById("carte"), options);

//övénement zone drag and drop
$("#drop-area").on('dragenter', function(e) {
    e.preventDefault();
    $(this).css('background', '#BBD5B8');
});
$("#drop-area").on('dragover', function(e) {
    e.preventDefault();
});
//Après avoir lâcher les photos dans la zone
$("#drop-area").on('drop', function(e) {
    $(this).css('background', '#D8F9D3');
    e.preventDefault();
    var image = e.originalEvent.dataTransfer.files;
    createFormData(image);
});
//Récupère chaque photo ajouter dans la zone
function createFormData(image) {

    var formImage = new FormData();
    for (i = 0; i < image.length; i++) {

        formImage.append('userImage' + i, image[i]);
    }

    uploadFormData(formImage);
}
//Récupération des données Exif
function uploadFormData(formData) {
    $.ajax({
        url: "uploadPhotos.php",
        type: "POST",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        success: function(data) {
            //Affichage pour le choix du point à associer et de la description
            $('#content').html(data);
            $("#drop-area").css("display", "none");
        }
    });
}
$(function() {
// Initialisation du slider
    $("#slider").dateRangeSlider({
        bounds: {min: range_db, max: range_fin},
        defaultValues: {min: range_db, max: range_fin},
        formatter: function(val) {
            var days = val.getDate(),
                    month = val.getMonth(),
                    year = val.getFullYear();
            return days + " " + months[month];
        },
        step: {
            days: 1
        },
        scales: [{
                first: function(value) {
                    return value;
                },
                end: function(value) {
                    return value;
                },
                next: function(value) {
                    var next = new Date(value);
                    return new Date(next.setMonth(value.getMonth() + 1));
                    //return new Date(next.setDate(value.getDate() + 1));
                },
                label: function(value) {
                    return months[value.getMonth()];
                    //return value.getDate()
                },
                format: function(tickContainer, tickStart, tickEnd) {
                    tickContainer.addClass("myCustomClass");
                }
            },
            {
                first: function(value) {
                    return value;
                },
                end: function(value) {
                    return value;
                },
                next: function(value) {
                    var next = new Date(value);
                    //return new Date(next.setMonth(value.getMonth() + 1));
                    return new Date(next.setDate(value.getDate() + 1));
                },
                label: function(value) {
                    //return months[value.getMonth()];
                    return value.getDate();
                },
                format: function(tickContainer, tickStart, tickEnd) {
                    tickContainer.addClass("myCustomClass");
                }
            }]
    });
    //Suppression de tous les trajets lors d'une modification de la période
    $("#slider").on("valuesChanging", function(e, data) {

        tab_line.forEach(function(tab_depPts) {
            tab_depPts.forEach(function(tab_arrPts) {
                tab_arrPts.forEach(function(tab_seg) {
                    tab_seg.forEach(function(segment) {
                        segment.setMap(null);
                    });
                });
            });

        });
        //Ajoute un événement à chaque point


//Si les valeurs de la barre changent récupère la valeur min et la valeur max
        var db = new Date(data.values.min);
        var fin = new Date(data.values.max);
        //Reformate la date
        var date_db = db.getFullYear() + "-" + (db.getMonth() + 1) + "-" + db.getDate();
        var date_fin = fin.getFullYear() + "-" + (fin.getMonth() + 1) + "-" + (fin.getDate());
        //Appel fonction qui affiche les points qui se trouvent entre la date min et la date max
        affiche_periode(date_db, date_fin, vge);

    });

});


/*****************************************************************************************************************
 * new_point
 * JHI / 20.2.15
 * Ajoute un point d'intérêt (icône personalisée) à la carte
 */
function new_point(lat, lng, type, text) {
    tab = new Array();
    tab['latLng'] = new google.maps.LatLng(lat, lng);
    tab['type'] = type;
    tab['text'] = text;
    tab['marqueur'] = new google.maps.Marker({
        map: carte,
        position: tab['latLng'],
        icon: icon_type(tab['type']),
        title: text
    });

    bounds.extend(new google.maps.LatLng(lat, lng));

    return tab;
} //new_point


function add_point(id, lat, lng, type, text) {

    if (typeof tab_pos[id] == 'undefined' || tab_pos[id] == -1) {
        console.log("ajout du point : " + id);
        tab_pos[id] = new Array();
        tab_pos[id] = new_point(lat, lng, type, text);

        //Ajout d'événement sur les points côté utilisateur
        if (updatePeriode === true) {
            tab_pos.forEach(function(d) {
                if (tab_pos[tab_pos.indexOf(d)] != -1) {
                    var marker = tab_pos[tab_pos.indexOf(d)]["marqueur"];
                    google.maps.event.addListener(marker, 'click', function() {
                        var formPhoto = new FormData();
                        formPhoto.append("id_pts", tab_pos.indexOf(d));
                        $.ajax({
                            url: "./json/allInfosPoint.json.php",
                            type: "POST",
                            data: formPhoto,
                            dataType: 'json',
                            contentType: false,
                            cache: false,
                            processData: false,
                            success: function(data) {
                                //Enlève le contenu des divs contenu dans la div info
                                $("#infos").html("");
                                data.forEach(function(d) {

                                    $("#infos").append("<div id=\"titre" + d.id_art + "\"></div>");
                                    //Explode les dates de début et fin du point
                                    if (typeof (d.date_heure_db_art) != "undefined" && typeof (d.date_heure_fin_art) != "undefined") {
                                        var tab_date_db = d.date_heure_db_art.substr(0, 10).split("-");
                                        var tab_date_fin = d.date_heure_fin_art.substr(0, 10).split("-");
                                        //Convertit les jours du mois en nom
                                        moisDb = months[parseInt(tab_date_db[1])];
                                        moisFin = months[parseInt(tab_date_fin[1])];
                                        //Jours début et fin égaux
                                        if (tab_date_db[2] === tab_date_fin[2]) {
                                            $("#titre" + d.id_art + "").append("<h3>" + tab_date_db[2] + " " + moisDb + " " + tab_date_fin[0] + "</h3>");
                                        }
                                        //Mois et année début et fin égaux
                                        else if (moisDb === moisFin && tab_date_db[0] === tab_date_fin[0]) {
                                            $("#titre" + d.id_art + "").append("<h3>" + tab_date_db[2] + " au " + tab_date_fin[2] + " " + moisDb + " " + tab_date_fin[0] + "</h3>");
                                        }
                                        //Mois début et fin égaux
                                        else if (moisDb === moisFin) {
                                            $("#titre" + d.id_art + "").append("<h3>" + tab_date_db[2] + " " + moisDb + " " + tab_date_db[0] + " au " + tab_date_fin[2] + " " + moisFin + " " + tab_date_fin[0] + "< /h3>");
                                        }
                                        //Année début et fin égales
                                        else if (tab_date_db[0] === tab_date_fin[0]) {
                                            $("#titre" + d.id_art + "").append("<h3>" + tab_date_db[2] + " " + moisDb + " au " + tab_date_fin[2] + " " + moisFin + " " + tab_date_fin[0] + "< /h3>");
                                        }

                                        //Ajoute l'article suelement si il existe
                                        if (typeof (d.article) != "undefined") {
                                            $("#infos").append("<div id=\"article" + d.id_art + "\"></div>");
                                            $("#article" + d.id_art + "").append(d.article + "<br />");
                                        }

                                    }


                                    //ajoute l'image seulement si elle existe
                                    if (typeof (d.nom_pht) != "undefined") {

                                        $("#infos").append("<div  id=\"gallery\"></div>");
                                        $("#gallery").append("<img class=\"photo\" onClick=\"affichePhoto('" + d.nom_pht + "','" + d.desc_pht + "')\" src=\"./php/images/" + d.nom_pht + "\"/>");
                                    }
                                });
                                //Ouvre la boîte de dialogue avec toutes les informations
                                $("#infos").dialog("open");
                            }
                        });
                    });

                }
            });
        }      
        return true;
    } else {
        bounds.extend(new google.maps.LatLng(lat, lng));
        return false;
    }

}

//Update les points
function affiche_periode(db, fin, voyage) {
    updatePeriode = true;
    var dataString = "db=" + db + "&fin=" + fin + "&voyage=" + voyage;
    //Initialise un tableau qui contiendra les nouveaux points
    var tab_new = new Array();

    $.ajax({
        type: "POST",
        url: "./json/periode.json.php",
        data: dataString,
        dataType: 'json',
        success: function(data) {
            bounds = new google.maps.LatLngBounds();
            $.each(data, function(key, pts) {
                //Ajoute tous les points contenu dans l'intervalle des dates sur la carte
                add_point(pts.id_pts, pts.lat_pts, pts.lng_pts, pts.type_pts, pts.nom_pts);
                tab_new[parseInt(pts.id_pts)] = parseInt(pts.id_pts);
            });
            tab_trj.forEach(function(t_trj) {
                if (tab_new.indexOf(t_trj['dp_trj']) >= 0 && tab_new.indexOf(t_trj['ar_trj']) >= 0) {
                    new_trajet(carte, t_trj['type_trj'], t_trj['dp_trj'], t_trj['ar_trj']);
                }
            })
            refresh_point(tab_new);
        }
    });
}
//Supprime les points après changement de période
function refresh_point(tab) {

    for (var key in tab_pos) {

        if (typeof tab[key] == 'undefined') {

            remove_point(key);

        }
    }

    //Ajoute un événement de suppression sur tous les trajets
    tab_line.forEach(function(tab_depPts) {
        if (typeof tab_depPts != 'undefined') {
            tab_depPts.forEach(function(tab_arrPts) {
                if (typeof tab_arrPts != 'undefined') {
                    tab_arrPts.forEach(function(tab_seg) {
                        tab_seg.forEach(function(segment) {
                            google.maps.event.addListener(segment, 'click', function() {
                                delIdTrj = tab_line.indexOf(tab_depPts);
                                $("#deleteTraj").dialog("open");
                            });
                        });
                    });
                }
            });
        }
    });

    carte.fitBounds(bounds);
}

/*****************************************************************************************************************
 * remove_point
 * JHI / 20.2.15
 * Ajoute un point d'intérêt (icône personalisée) à la carte
 */
function remove_point(id_pts) {
    var formTrj = new FormData();
    //Efface le point de la carte
    if (typeof tab_pos[id_pts] != 'undefined' && tab_pos[id_pts] != -1) {
        tab_pos[id_pts]['marqueur'].setMap(null);
        if (updatePeriode === false) {
            //Si le point supprimé fait partie d'une arrivée de trajet
            tab_line.forEach(function(tab_depPts) {
                tab_depPts.forEach(function(tab_arrPts) {
                    for (arr_id in tab_arrPts) {

                        if (parseInt(arr_id) === parseInt(id_pts)) {

                            for (dep_id in tab_depPts) {

                                formTrj.append("dp_trj", parseInt(dep_id));
                            }

                            formTrj.append("ar_trj", parseInt(arr_id));

                            $.ajax({
                                url: "../json/delTrajet.json.php",
                                type: "POST",
                                data: formTrj,
                                dataType: 'json',
                                contentType: false,
                                cache: false,
                                processData: false,
                                success: function() {
                                }
                            });

                            tab_arrPts.forEach(function(tab_seg) {
                                tab_seg.forEach(function(segment) {
                                    segment.setMap(null);

                                });
                            });

                        }
                    }

                });

            });
            //Si le point supprimé fait partie d'un départ de trajet
            tab_line.forEach(function(tab_depPts) {
                for (dep_id in tab_depPts) {
                    if (parseInt(dep_id) === parseInt(id_pts)) {
                        console.log("id " + dep_id);
                        formTrj.append("dp_trj", parseInt(dep_id));
                        tab_depPts.forEach(function(tab_arrPts) {
                            for (arr_id in tab_arrPts) {
                                formTrj.append("ar_trj", parseInt(arr_id));
                            }
                            $.ajax({
                                url: "../json/delTrajet.json.php",
                                type: "POST",
                                data: formTrj,
                                dataType: 'json',
                                contentType: false,
                                cache: false,
                                processData: false,
                                success: function() {
                                }
                            });
                            tab_arrPts.forEach(function(tab_seg) {
                                tab_seg.forEach(function(segment) {
                                    segment.setMap(null);

                                });
                            });

                        });

                    }
                }
            });
        }


        console.log("remove du point : " + id_pts);
        tab_pos[id_pts] = -1;

    }
}
/*****************************************************************************************************************
 * new_trajet
 * JHI / 20.2.15
 * Ajoute un trajet (curved line + icône de direction) à la carte
 */
function new_trajet(map, type, id_pt_1, id_pt_2) {

    if (typeof id_pt_1 === "undefined") {
        pt_1 = tab_pos[tab_pos.length - 2];
    } else {
        pt_1 = tab_pos[id_pt_1];
    }
    if (typeof id_pt_2 === "undefined") {
        pt_2 = tab_pos[tab_pos.length - 1];
    } else {
        pt_2 = tab_pos[id_pt_2];
    }

    // marker_1 = pt_1['marqueur'];
    //   bounds.extend(marker_1.getPosition());
    //marker_2 = pt_2['marqueur'];
    //   bounds.extend(marker_2.getPosition());

    curved_line_generate(map, {pstart: pt_1, pstop: pt_2, type: type, id_pt_1: id_pt_1, id_pt_2: id_pt_2});
} // new_trajet

/*****************************************************************************************************************
 * icon type
 * JHI / 20.2.15
 * fournit les options nécessaire à l'affichage d'une icône en fonction du type donné
 */
function icon_type(type) {
    if (type != "") {
        icon = {url: "http://www.google.com/mapfiles/ms/micons/" + type + ".png",
            size: new google.maps.Size(40, 40),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(20, 20),
            scale: 0.1
        };
    } else {
        icon = {url: "http://www.google.com/mapfiles/ms/micons/red-dot.png",
            size: new google.maps.Size(40, 40),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(20, 20),
            scale: 0.1
        };
    }
    return icon;
} //icon_type

//Redirige vers une autre page
function changePage(name) {
    window.location = "./" + name + ".php";
}

//Ajoute un événement à tous les points permettant de soit le supprimer ou d'ajouter un trajet
function marker_update(idPts) {
    //Récupère le marqueur de l'id du point envoyé
    var marker = tab_pos[idPts]["marqueur"];
    //Lui ajoute un événement
    google.maps.event.addListener(marker, 'click', function() {
        //Si un nouveau voyage n'est PAS en cours de création
        if ($("#newTrj").is(":not(:checked)")) {
            ptsSelected = idPts;
            $("#optPoint").dialog("open");
        }
        //Si un voyage est en cours de création
        else {
            //Si le premier point n'est PAS le même que le deuxième
            if (id_arr === "" && id_dep !== "" && id_dep !== idPts) {
                id_arr = idPts;
                $("#addTrajet").dialog("open");
            }
            //Si le premier point n'est PAS le même que le deuxième
            else {
                $("#optPoint").dialog("open");
            }
        }
    });
}
//Ajoute un événement à chaque trajet pe^rmettant de le supprimer
function update_event_trj() {
    tab_line.forEach(function(tab_depPts) {
        if (typeof tab_depPts != 'undefined') {
            tab_depPts.forEach(function(tab_arrPts) {
                if (typeof tab_arrPts != 'undefined') {
                    tab_arrPts.forEach(function(tab_seg) {
                        tab_seg.forEach(function(segment) {
                            google.maps.event.addListener(segment, 'click', function() {
                                delIdTrj = tab_line.indexOf(tab_depPts);
                                $("#deleteTraj").dialog("open");
                            });
                        });

                    });
                }
            });
        }
    });
}
//Affiche une image agrandie avec sa légende
function affichePhoto(nomPhoto, descPhoto) {
    $("#photo").html("");
    $("#photo").html("<div id=\"infoPhoto\"><img src=\"./php/images/" + nomPhoto + "\"/></div>");
    $("#infoPhoto").after(descPhoto);
    $("#photo").dialog("open");
}
initialiser();


