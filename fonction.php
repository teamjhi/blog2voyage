<?php

/* * *******************************************************
 *                 Concerne les voyages                  *
 * ******************************************************* */

//Retourne tous les voyages déjà existants
function allTravels() {
    require("connect.inc.php");
    $tab_vge = $pdo->query("SELECT nom_vge, id_vge FROM t_voyage")->fetchAll(PDO::FETCH_ASSOC);
    return $tab_vge;
}

//Retourne le nom d'un voyage
function nameTravel($idTravel) {
    require("connect.inc.php");
    $nameTra = $pdo->query("SELECT nom_vge FROM t_voyage WHERE id_vge=" . $idTravel)->fetch(PDO::FETCH_ASSOC);
    return $nameTra["nom_vge"];
}

//Ajoute un voyage dans la base et retourne le dernier ID insérer dans la base t_voyage
function addTravel($nameTravel) {
    require("connect.inc.php");
    $query = "INSERT INTO `t_voyage`(`nom_vge`) VALUES (\"" . $nameTravel . "\")";
    $pdo->query($query)->fetch(PDO::FETCH_ASSOC);
}

function lastIdTravel() {
    require("connect.inc.php");
    $query = "SELECT MAX(id_vge) AS idVge FROM t_voyage";
    $tab_idVge = $pdo->query($query)->fetch(PDO::FETCH_ASSOC);
    return $tab_idVge["idVge"];
}

/* * *******************************************************
 *                 Concerne les points                   *
 * ******************************************************* */

//Retourne tous les points d'un voyage
function travelPoints($idTravel) {
    require("connect.inc.php");

    $tab_pts = $pdo->query("SELECT * FROM t_points WHERE id_vge=" . $idTravel)->fetchAll(PDO::FETCH_ASSOC);
    return $tab_pts;
}

//Retourne les informations d'un point permettant le tri des points proposés pour les photos
function infoPoint($name) {
    require("connect.inc.php");
    $tab_pts = $pdo->query("SELECT * FROM t_points NATURAL JOIN t_articles WHERE nom_pts=\"" . $name . "\"")->fetch(PDO::FETCH_ASSOC);
    return $tab_pts;
}

/* * *******************************************************
 *                 Concerne les photos                   *
 * ******************************************************* */

//Classe et retourne une la liste de point dans l'ordre des coordonnées GPS les plus proches de la photo
function classementPoints($idTravel, $lat, $long) {
    require("connect.inc.php");
    $query = "SELECT *,(ABS(`lat_pts`-" . $lat . ") + ABS(`lng_pts`-" . $long . ")/2) AS ratio FROM `t_points` WHERE `id_vge`=" . $idTravel . " ORDER BY ratio";
    $tab_class = $pdo->query($query);
    return $tab_class;
}

//Trier les photos par dates selon les articles si aucune coordonnée GPS n'est connue
function classementDate($idTravel, $date) {
    require("connect.inc.php");
    $query = "SELECT *,ABS(DATEDIFF(\"" . $date . "\",date_heure_db_art)) AS diffDate FROM t_articles NATURAL JOIN t_points WHERE id_vge=" . $idTravel . " GROUP BY nom_pts ORDER BY diffDate";
    $tab_date = $pdo->query($query);
    return $tab_date;
}

//Ajoute une photo dans la base
function addPhoto($lat, $long, $desc, $idPts, $date, $nomPhoto) {
    require("connect.inc.php");
    $query = "INSERT INTO `t_photos`(`lat_pht`, `lng_pht`, `desc_pht`, `id_pts`, `date_heure_pht`,nom_pht) VALUES (\"" . $lat . "\",\"" . $long . "\",\"" . $desc . "\",\"" . $idPts . "\",\"" . $date . "\",\"" . $nomPhoto . "\")";
    $pdo->query($query);
}

//Met à jour les données d'une photo
function updatePhoto($lat, $long, $desc, $idPts, $date, $nomPhoto) {
    require("connect.inc.php");
    $query = "UPDATE `t_photos` SET `lat_pht`=" . $lat . ",`lng_pht`=" . $long . ",`desc_pht`=\"" . $desc . "\",`id_pts`=" . $idPts . ",`date_heure_pht`=\"" . $date . "\",`nom_pht`=\"" . $nomPhoto . "\" WHERE nom_pht=\"" . $nomPhoto . "\"";
    $pdo->query($query);
}

//Retourne l'ID de la dernière photo ajoutée
function lastIdPhoto() {
    require("connect.inc.php");
    $query = "SELECT MAX(id_pht) FROM t_photos";
    $id = $pdo->query($query)->fetch(PDO::FETCH_ASSOC);
    return $id["MAX(id_pht)"];
}

//Supprime une photo
function deletePhoto($nomPht) {
    require("connect.inc.php");
    $query = "DELETE FROM `t_photos` WHERE nom_pht=\"" . $nomPht . "\"";
    $pdo->query($query);
}

//Retourne toutes les photos d'un point
function photosPoint($idPts) {
    require("connect.inc.php");
    $query = "SELECT * FROM t_photos WHERE id_pts=" . $idPts;
    $tab_photos = $pdo->query($query);
    return $tab_photos;
}

/* * *******************************************************
 *                 Concerne les articles                 *
 * ******************************************************* */

//Ajoute un article à la base
function addArticle($idTravel, $text, $dateDb, $dateFin) {
    require("connect.inc.php");
    $query = "INSERT INTO `t_articles`(`date_heure_db_art`, `date_heure_fin_art`, `article`, `id_pts`) VALUES (\"" . $dateDb . "\",\"" . $dateFin . "\",\"" . $text . "\"," . $idTravel . ")";
    $pdo->query($query);
}

//Retourne tous les articles d'un point
function articlesPoint($idPts) {
    require("connect.inc.php");
    $query = "SELECT * FROM t_articles WHERE id_pts=" . $idPts;
    $tabArticles = $pdo->query($query);
    return $tabArticles;
}

//Met à jour un article après modification
function updateArticle($id_art, $dateDb, $dateFin, $article, $id_pts) {
    require("connect.inc.php");
    $query = ("UPDATE `t_articles` SET `date_heure_db_art`=\"" . $dateDb . "\",`date_heure_fin_art`=\"" . $dateFin . "\",`article`=\"" . $article . "\",`id_pts`=\"" . $id_pts . "\" WHERE id_art=" . $id_art);
    $pdo->query($query);
}

//Supprime un article dans la base
function deleteArticle($idArt) {
    require("connect.inc.php");
    $query = ("DELETE FROM t_articles WHERE id_art=" . $idArt);
    $pdo->query($query);
}

/* * *******************************************************
 *                        Divers                         *
 * ******************************************************* */

//Enlève toutes les accentuations et met le texte en minuscule (nom des photos)
function minusculesSansAccents($texte) {
    $texte = mb_strtolower($texte, 'UTF-8');
    $texte = str_replace(
            array(
        'à', 'â', 'ä', 'á', 'ã', 'å',
        'î', 'ï', 'ì', 'í',
        'ô', 'ö', 'ò', 'ó', 'õ', 'ø',
        'ù', 'û', 'ü', 'ú',
        'é', 'è', 'ê', 'ë',
        'ç', 'ÿ', 'ñ',
            ), array(
        'a', 'a', 'a', 'a', 'a', 'a',
        'i', 'i', 'i', 'i',
        'o', 'o', 'o', 'o', 'o', 'o',
        'u', 'u', 'u', 'u',
        'e', 'e', 'e', 'e',
        'c', 'y', 'n',
            ), $texte
    );

    return $texte;
}

//Affiche le menu de la page admin.php
function showButtons() {
    echo "<table><td><input type=\"button\" value=\"Ajouter/Modifier des articles\"  onclick=\"window.location ='articles.php'\" class=\"btn btn-default\"/></td>";
    echo "<td><input type=\"button\" value=\"Modifier des photos\"  onclick=\"window.location ='photos.php'\" class=\"btn btn-default\"/></td>";
    echo "<td><input type=\"button\" value=\"Affichage\" onclick=\"window.location =' ../index.php'\" class=\"btn btn-default\"/></td>"
    . "</tr></table>";
}

//Transforme les données GPS exif des photos ex: (20°N) en (20,00)
function DMStoDD($deg, $min, $sec, $ref) {
    $deg = explode("/", $deg);
    $deg = $deg[0] / $deg[1];

    $min = explode("/", $min);
    $min = $min[0] / $min[1];

    $sec = explode("/", $sec);
    $sec = $sec[0] / $sec[1];


    $degre = $deg + ((($min * 60) + ($sec)) / 3600);
    //Si la coordonnée se trouve au Sud ou à l'ouest inversion des degrès
    if ($ref === "S" || $ref === "O") {
        $degre*=-1;
    }

    return $degre;
}
