<?php
session_start();
require("./connect.inc.php");
require("./fonction.php");
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">

    <head>
        <title>Blog2voyage</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!-- El�ment Google Maps indiquant que la carte doit �tre affich� en plein �cran et
        qu'elle ne peut pas �tre redimensionn�e par l'utilisateur -->
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
        <!-- Inclusion de l'API Google MAPS -->
        <!-- Le param�tre "sensor" indique si cette application utilise d�tecteur pour d�terminer la position de l'utilisateur -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
            <link rel="stylesheet" href="./css/style.css" type="text/css" />
            <link rel="stylesheet" type="text/css" href="./icon/css/map-icons.css" />
            <link rel="stylesheet" type="text/css" href="./jquery-ui/jquery-ui.css" />
            <link rel="stylesheet" href="./jQRangeSlider/css/iThing.css" type="text/css" />
            <style>              
                body{
                    background-color:#B9C3E0;
                }

                #voyage{
                    width:100px;
                    height:30px;
                    font-size: 17px;
                }
            </style>


            <script src="./jquery/jquery.min.js"></script>
            <script src="./jquery-ui/jquery-ui.js"></script>
            <script src="./jQRangeSlider/jQRangeSlider-min.js"></script>
            <script src="./jQRangeSlider/jQDateRangeSlider-withRuler-min.js"></script>
            <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>-->
            <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
            <script type="text/javascript" src="curved_line.js"></script>



            <script>
                var tab_pos = new Array();
                var tab_line = new Array();
                var marker = new Array();
                var tab_new = new Array();
                var carte;
                var range_db;
                var range_fin;
                var bounds = new google.maps.LatLngBounds();
                TILE_SIZE = 256;
                var id_pts;
                var months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
                var moisDb;
                var moisFin;
                var vge;
<?php
if (isset($_POST["voyage"])) {
    //récupére l'id envoyé

    $id_vge = $_POST["voyage"];
    $_SESSION["id_vge"] = $id_vge;
} else if (isset($_SESSION["id_vge"])) {
    $id_vge = $_SESSION["id_vge"];
}
//Voyage par défaut
else {
    $id_vge = 5;
}
$tab_trj;

$tab_trj = $pdo->query("SELECT * FROM t_trajets JOIN t_points ON dp_trj=id_pts WHERE id_vge=" . $id_vge)->fetchAll(PDO::FETCH_ASSOC);
// Recherche de la date min et max du voyage  
$tab_range = $pdo->query("SELECT MIN(date_heure_db_art) AS date_db ,Max(date_heure_fin_art) AS date_fin FROM t_points NATURAL JOIN t_articles WHERE date_heure_db_art<>'00-00-00 00:00:00' AND date_heure_fin_art<>'00-00-00 00:00:00' AND id_vge=" . $id_vge)->fetch(PDO::FETCH_ASSOC);
//print_r($tab_range);
if (sizeof($tab_range)) {
    $t = strtotime($tab_range["date_db"]);
    echo "range_db= new Date(" . date("Y", $t) . "," . (date("m", $t) - 1) . "," . date("d", $t) . ");";
    $t = strtotime($tab_range["date_fin"]);
    echo "range_fin= new Date(" . date("Y", $t) . "," . (date("m", $t) - 1) . "," . date("d", $t) . ");";
}
?>

                var tab_trj = new Array();
                function initialiser() {

<?php
$tab_pts = $pdo->query("SELECT * FROM t_points WHERE id_vge=" . $id_vge . "")->fetchAll(PDO::FETCH_ASSOC);
foreach ($tab_pts AS $pts) {

    echo "tab_pos[" . $pts['id_pts'] . "] = new Array();\n";
    echo "tab_pos[" . $pts['id_pts'] . "] = new_point(" . $pts['lat_pts'] . "," . $pts['lng_pts'] . ",\"" . $pts['type_pts'] . "\",\"" . $pts['nom_pts'] . "\");\n";
}

$tab_trj = $pdo->query("SELECT * FROM t_trajets JOIN t_points ON dp_trj=id_pts WHERE id_vge=" . $id_vge)->fetchAll(PDO::FETCH_ASSOC);
if (sizeof($tab_trj)) {
    foreach ($tab_trj as $key => $trj) {

        echo "new_trajet(carte,\"" . $trj['type_trj'] . "\"," . $trj['dp_trj'] . "," . $trj['ar_trj'] . ");\n";
        echo "tab_trj[" . $key . "]=new Array();\n";
        echo "tab_trj[" . $key . "]['type_trj']=\"" . $trj['type_trj'] . "\";\n";
        echo "tab_trj[" . $key . "]['dp_trj']=" . $trj['dp_trj'] . ";\n";
        echo "tab_trj[" . $key . "]['ar_trj']=" . $trj['ar_trj'] . ";\n";
    }
}
?>
                    // Adapte la carte à l'écran et au contenu
                    carte.fitBounds(bounds);
                    vge =<?php echo $id_vge; ?>;
                    //Sélectionne le bon voyage dans la liste déroulante
                    $("#voyage option[value=" + vge + "]").prop("selected", true);
                    //Changement de voyage
                    $('#voyage').change(function () {
                        $("#formVoyage").submit();
                    });

                    //Initialisation de la boîte de dialogue contenant les informations d'un point
                    $("#infos").dialog({
                        autoOpen: false,
                        width: 1000,
                        height: 500

                    });
                    //Initialisation de la boîte de dialogue contenant une image agrandie
                    $("#photo").dialog({
                        autoOpen: false,
                        modal: true,
                        width: 'auto',
                        height: 'auto'
                    });
                    //Ajout d'un événement à chaque point

                    tab_pos.forEach(function (d) {
                        var marker = tab_pos[tab_pos.indexOf(d)]["marqueur"];
                        google.maps.event.addListener(marker, 'click', function () {
                            var formPhoto = new FormData();
                            formPhoto.append("id_pts", tab_pos.indexOf(d));
                            $.ajax({
                                url: "./json/allInfosPoint.json.php",
                                type: "POST",
                                data: formPhoto,
                                dataType: 'json',
                                contentType: false,
                                cache: false,
                                processData: false,
                                success: function (data) {
                                    //Enlève le contenu des divs contenu dans la div info
                                    $("#infos").html("");
                                    data.forEach(function (d) {

                                        $("#infos").append("<div id=\"titre" + d.id_art + "\"></div>");
                                        //Explode les dates de début et fin du point
                                        if (typeof (d.date_heure_db_art) != "undefined" && typeof (d.date_heure_fin_art) != "undefined") {
                                            var tab_date_db = d.date_heure_db_art.substr(0, 10).split("-");
                                            var tab_date_fin = d.date_heure_fin_art.substr(0, 10).split("-");
                                            //Convertit les jours du mois en nom
                                            moisDb = months[parseInt(tab_date_db[1])];
                                            moisFin = months[parseInt(tab_date_fin[1])];
                                            //Jours début et fin égaux
                                            if (tab_date_db[2] === tab_date_fin[2]) {
                                                $("#titre" + d.id_art + "").append("<h3>" + tab_date_db[2] + " " + moisDb + " " + tab_date_fin[0] + "</h3>");
                                            }
                                            //Mois et année début et fin égaux
                                            else if (moisDb === moisFin && tab_date_db[0] === tab_date_fin[0]) {
                                                $("#titre" + d.id_art + "").append("<h3>" + tab_date_db[2] + " au " + tab_date_fin[2] + " " + moisDb + " " + tab_date_fin[0] + "</h3>");
                                            }
                                            //Mois début et fin égaux
                                            else if (moisDb === moisFin) {
                                                $("#titre" + d.id_art + "").append("<h3>" + tab_date_db[2] + " " + moisDb + " " + tab_date_db[0] + " au " + tab_date_fin[2] + " " + moisFin + " " + tab_date_fin[0] + "< /h3>");
                                            }
                                            //Année début et fin égales
                                            else if (tab_date_db[0] === tab_date_fin[0]) {
                                                $("#titre" + d.id_art + "").append("<h3>" + tab_date_db[2] + " " + moisDb + " au " + tab_date_fin[2] + " " + moisFin + " " + tab_date_fin[0] + "< /h3>");
                                            }

                                            //Ajoute l'article suelement si il existe
                                            if (typeof (d.article) != "undefined") {
                                                $("#infos").append("<div id=\"article" + d.id_art + "\"></div>");
                                                $("#article" + d.id_art + "").append(d.article + "<br />");
                                            }

                                        }


                                        //ajoute l'image seulement si elle existe
                                        if (typeof (d.nom_pht) != "undefined") {

                                            $("#infos").append("<div  id=\"gallery\"></div>");
                                            $("#gallery").append("<img class=\"photo\" onClick=\"affichePhoto('" + d.nom_pht + "','" + d.desc_pht + "')\" src=\"./php/images/" + d.nom_pht + "\"/>");
                                        }
                                    });
                                    //Ouvre la boîte de dialogue avec toutes les informations
                                    $("#infos").dialog("open");
                                }
                            });
                        });
                    });

                }
            </script>
    </head>
    <!-- Structure DOM  -->
    <body>
        <div class="container-fluid">
            <?php
            //Choix du voyage
            echo "<br /><form id=\"formVoyage\" method=\"post\" action=\"\">";
            echo "<select id=\"voyage\" name=\"voyage\">";
            foreach (allTravels() as $key => $value) {
                echo "<option value=\"" . $value["id_vge"] . "\"/>" . $value["nom_vge"] . "</option>";
            }
            echo "<option  value=\"new\" />Nouveau voyage </option>";
            echo "</select></form>";
            ?>
            <input type="button" value="Administrateur" onClick="changePage('php/admin')" class="btn btn-default"/>
            <div class="row">
                <div class="col-md-12">
                    <div id="carte" style="width:100%; height:85%; margin-top:20px;"></div>
                </div>
            </div>



            <div class="row">
                <div class="col-md-10">
                    <div id="slider">                                     
                    </div>
                </div>
            </div>
        </div>
        <div id="infos">                                     
        </div>
        <div id="photo">                                     
        </div>
    </body>
</html>
<script src="./fonction.js"></script>

