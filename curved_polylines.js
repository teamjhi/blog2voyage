/* 
 
	Simple Javascript Curved Line function for use with Google Maps Api 
	
	author: Daniel Nanovski
	version: 0.0.1 (Beta)
	website: http://curved_lines.overfx.net/
	
	License:
	Copyright (c) 2012 Daniel Nanovski, http://overfx.net/

	Permission is hereby granted, free of charge, to any person obtaining
	a copy of this software and associated documentation files (the
	"Software"), to deal in the Software without restriction, including
	without limitation the rights to use, copy, modify, merge, publish,
	distribute, sublicense, and/or sell copies of the Software, and to
	permit persons to whom the Software is furnished to do so, subject to
	the following conditions:
	
	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
	LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
	WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
*/


function curved_line_generate(LatStart, LngStart, LatEnd, LngEnd, Color, Horizontal, Multiplier) {
	
	var LastLat = LatStart;
	var LastLng = LngStart;
	
	var PartLat;
	var PartLng;
	
	var Points = new Array(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9);
	var PointsOffset = new Array(0.2, 0.35, 0.5, 0.55, 0.60, 0.55, 0.5, 0.35, 0.2);
	
	var OffsetMultiplier = 0;
	
	if(Horizontal == 1) {
	
		var OffsetLenght = (LngEnd - LngStart) * 0.1;
	
	} else {
		
		var OffsetLenght = (LatEnd - LatStart) * 0.1;
		
	}
			 
	for(var i = 0; i < Points.length; i++) {
		
		if(i == 4) {
			
			OffsetMultiplier = 1.5 * Multiplier;
			
		}
		
		if(i >= 5) {
			
			OffsetMultiplier = (OffsetLenght * PointsOffset[i]) * Multiplier;
			
		} else {
		
			OffsetMultiplier = (OffsetLenght * PointsOffset[i]) * Multiplier;
			
		}
		
		if(Horizontal == 1) {
		
			PartLat = (LatStart + ((LatEnd - LatStart) * Points[i])) + OffsetMultiplier;
			PartLng = (LngStart + ((LngEnd - LngStart) * Points[i]));
		
		} else {
			
			PartLat = (LatStart + ((LatEnd - LatStart) * Points[i]));
			PartLng = (LngStart + ((LngEnd - LngStart) * Points[i])) + OffsetMultiplier;
			
		}
		
		curved_line_create_segment(LastLat, LastLng, PartLat, PartLng, Color);
		
		LastLat = PartLat;
		LastLng = PartLng;
	
	}
	
	curved_line_create_segment(LastLat, LastLng, LatEnd, LngEnd, Color);
	
}

